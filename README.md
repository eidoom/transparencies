# [Transparencies](https://gitlab.com/eidoom/transparencies)
[Live here](https://eidoom.gitlab.io/transparencies/)
## Usage
```shell
find . -type f ! -path "./.*" | entr ./build.py
```
```shell
live-server public
```
## Notes
* On GitLab CI, there's a `pip` and `venv` cache as in <https://docs.gitlab.com/ee/ci/caching/#cache-python-dependencies>
* Open Sans font (subject to change?) from <https://github.com/googlefonts/opensans/tree/main/fonts/variable> and processed as in <https://computing-blog.netlify.app/post/zola/#font> - `wdth` won't go beyond `font-stretch: 100%`?
