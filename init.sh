echo "Note that this must be run as \`source init.sh\`"

if [ ! -d ".venv" ]; then
	python3 -m venv .venv
	source .venv/bin/activate
	if [ -f "requirements.txt" ]; then
		python3 -m pip install -r requirements.txt
	fi
else
	source .venv/bin/activate
fi

echo "venv activated"
echo "use \`deactivate\` when done"
