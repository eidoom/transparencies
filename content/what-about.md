---
title: What about...
id: what-about
next: how
---

OK, web slides already exist.

For [example](https://eidoom.gitlab.io/ytf20), you can use `pandoc` to compile `reveal.js` slides.

But I want something minimal and hackable---and it's easy to throw together.
