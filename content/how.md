---
title: How?
id: how
---

Metadata is entered in `yaml` and content is typed up in `markdown`.

Layout is controlled with `jinja2` templates, while styling is applied from `css`.

The `python` script generates the `html` and the browser handles the rest.
